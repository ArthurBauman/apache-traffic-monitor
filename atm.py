#!/usr/bin/env python3

import config
from datetime import datetime
from glob import glob
import gzip
import json
import smtplib
import ssl
import socket

path = config.log_path
vhosts = config.vhosts
current_month = datetime.now().month
email_text = ''

for vhost in vhosts:
    traffic_byte = 0
    vhost_name = vhost[0]
    for filename in glob(path + vhost_name + '_access.log*'):
        if filename.endswith('.gz'):
            try:
                file_object = gzip.open(filename, 'rt')
            except Exception as e:
                print(e)
        else:
            try:
                file_object = open(filename, 'r')
            except Exception as e:
                print(e)
        for line in file_object:
            try:
                json_line = json.loads(line)
            except Exception as e:
                print(e)
            date_time = json_line['dateTime']
            date_time_obj = datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S')
            log_month = date_time_obj.date().month
            if log_month == current_month:
                traffic_byte += int(json_line['bytesSent'])
        file_object.close()
    traffic_gbyte = round(float(traffic_byte)/(1024*1024), 3)
    email_text = \
        email_text\
        + vhost_name\
        + ': '\
        + str(traffic_gbyte)\
        + 'GB traffic this month. Max traffic: '\
        + str(vhost[1])\
        + 'GB.\n'

message = """\
Subject: Traffic monitor """ + socket.gethostname() + """

Dear Sir/Madam,

This month traffic usage.

""" + email_text + """

# Trailing white space in sig seperator!
# https://www.karelbemelmans.com/2015/05/netiquette-email-signature-separator/

-- """ + """
Best regards,
The webmaster"""

# Create a secure SSL context
context = ssl.create_default_context()

# Try to log in to server and send email
try:
    server = smtplib.SMTP(config.smtp_server, config.port)
    server.ehlo()  # Can be omitted
    server.starttls(context=context)  # Secure the connection
    server.ehlo()  # Can be omitted
    server.login(config.user, config.password)
    server.sendmail(config.sender_email, config.receiver_email, message)
except Exception as e:
    # Print any error messages to stdout.
    # ToDo: Write to log file.
    print(e)
finally:
    server.quit()
