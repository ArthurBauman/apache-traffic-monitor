## Apache Monthly Traffic Monitor

*In god we trust, all others we monitor.*

---

This Python 3 script measures the traffic each Apache webserver virtual host montly uploads. The results will be emailed.

## Prerequisites

Python 3 must be installed.

This script expect a JSON log format with bytes sent (%O).  
E.g. in /etc/apache2/apache2.conf:  
LogFormat "{ \"dateTime\":\"%{%F %X}t\", \"remoteIp\":\"%a\", \"host\":\"%V\", \"request\":\"%U\", \"query\":\"%q\", \"method\":\"%m\", \"status\":\"%>s\", \"userAgent\":\"%{User-agent}i\", \"referer\":\"%{Referer}i\", \"bytesSent\":\"%O\" }" custom_json

Keep more than 1 month of log files.

Name your log files like this: example.com_access.log

## Installing

* git clone https://ArthurBauman@bitbucket.org/ArthurBauman/apache-traffic-monitor.git
* Edit config.py.example
* Rename config.py.example to config.py

## Using

Call:  
```python3 atm.py```  
or  
```./atm.py```

Preferable called by cron.

## Built With

* [Visual Studio Code](https://code.visualstudio.com/) - Code Editing Redefined.
* [Pylint](https://www.pylint.org/) - Code analysis for Python.
* [pycodestyle (formerly called pep8)](https://pypi.org/project/pycodestyle/) - Python style guide checker.



## Versioning

For the versions available, see the tags on this repository 

## Authors

Arthur Bauman - [https://bauman.nu](https://bauman.nu)


## License

No license, free to use.

